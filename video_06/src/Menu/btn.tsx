import { useState } from 'react';
import styled from 'styled-components';


export const Container = styled.div`
    border-radius:50%;
    border:1.8px solid white;
    height:60px;
    width:60px
    cursor:pointer;
    background-color: white;
    margin-left:15;
    
`
export const Icon = styled.img`
    height: 100%;
    width: 100%;
    object-fit:contain;
    border-radius: 50%;
`
export interface params{
    image:string;
    onClick ?: () => void;
}

const App = (params: params):JSX.Element => {
  
    //const [visible,setVisible] = useState(false);

    const handleClick = () => {
        //setVisible(!visible);
        if (typeof params.onClick === 'function') params.onClick();
    };

    return (
        <Container onClick={handleClick}>
            <Icon src={params.image}></Icon>
        </Container>
    );
  };
  
  export default App;