import { useEffect, useState } from 'react';
import styled from 'styled-components';


export const Container = styled.div<{visible: boolean}>`
    border-radius:50%;
    border:1.8px solid white;
    height:35px;
    width:35px;
    cursor:pointer;
    background-color: ${props => props.visible ? 'white':'transparent'};
`
export const Icon = styled.img<{visible: boolean}>`
    height: 35px;
    width: 35px;
    border-radius: 50px;
    visibility: ${props => props.visible ? 'visible':'hidden'};
`
export interface params{
    image:string;
    onClick ?: (value:boolean) => void;
    defaultValue: boolean;
}

const App = (params: params):JSX.Element => {
  
    const [visible,setVisible] = useState<boolean>(params.defaultValue);

    useEffect(() =>{
        setVisible(params.defaultValue)
    },[params.defaultValue]);
    const handleClick = () => {
        setVisible(!visible);
        if (typeof params.onClick === 'function') params.onClick(!visible);
    };

    return (
        <Container visible={visible} onClick={handleClick}>
            <Icon src={params.image} visible={visible}></Icon>
        </Container>
    );
  };
  
  App.defaultProps={
    defaultValue : false
  }

  export default App;