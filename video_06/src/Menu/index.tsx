import Checkbox from './checkbox';
import Option,{params as paramsComponentOption} from './option';
import styled from 'styled-components';
import Footer from './footer';
import { useEffect, useState } from 'react';

const Container = styled.div`
  margin-top: 15px;
  background-color: ${props => props.color};
  border-radius: 25px;
  width:400px;
  padding-top:15px;
`

export interface option{
  value:string;
  state:boolean;
}

export interface params {
  options:string[],
  backgroundColor ?: string;
  onAcept : (Options:option[]) => void;
  onReset : (Options:option[]) => void;
}
  
const App = (params:params) : JSX.Element => {
  const [options, setOptions] = useState<{value:string, state:boolean}[]>([]);
  
  useEffect(() => {
    var raw : option[] =[];
    params.options.forEach(el=> raw.push({value:el,state:false}))
    setOptions(raw);
  },[params.options]);

  const handleClick: paramsComponentOption['onClick'] = (e)=> {
    //console.log(e);
    const copy:option[] =[...options];
    const index:number = copy.findIndex(f => f.value === e.value);
    if (index > -1) copy[index] = e;
    setOptions(copy);
    //console.log(copy)  ;
  }

  const handleAcept = () => {
    if (typeof params.onAcept === 'function') {
      params.onAcept(options);
    }
  }

  const handleReset = () => {
    if (typeof params.onReset === 'function') {
      var raw : option[] =[];
      params.options.forEach(el=> raw.push({value:el,state:false}))
      setOptions(raw);
      params.onAcept(raw);
      //console.log(raw);
    }
  }

  return (
    <Container color={params.backgroundColor}>
      {options.map((v,i) => <Option key={i} value={v.value} defaultState={v.state} onClick={handleClick} />)}
      <Footer onAccept={handleAcept} onReset={handleReset} />
    </Container>
  );
};

export default App;