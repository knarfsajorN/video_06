
import styled from "styled-components";
import Checkbox from './checkbox';

import check from '../images/ok.png';

const Container = styled.div`
   margin-left:15px;
   margin-top:15px;
   display:flex;
   display-direction:row;
   justify-content:space-between;
   margin-right:25px;
   align-items:center
`;

const Title = styled.p`
    margin:0px;
    color: white;
    font-weight:500;
`

export const Circle = styled.div<{visible: boolean}>`
    border-radius:50%;
    border:1.8px solid white;
    height:35px;
    width:35px;
    cursor:pointer;
    background-color: ${props => props.visible ? 'white':'transparent'};
`
export const Icon = styled.img<{visible: boolean}>`
    height: 35px;
    width: 35px;
    border-radius: 50px;
    visibility: ${props => props.visible ? 'visible':'hidden'};
`

export interface params{
    value: string;
    defaultState: boolean;
    onClick ?: (params: {value: string,state:boolean}) => void;
}

const a = () => {
    console.log(a);
};

const App = (params : params) : JSX.Element =>{

    const handleClick = (state:boolean) => {
        if (typeof params.onClick === 'function') {
            params.onClick({value:params.value,state});
        }
    };

    return (
        <Container>
            <Title>
                {params.value}
            </Title>
            <Checkbox image={check} defaultValue={params.defaultState} onClick={(handleClick)}/>
        </Container>
    );
};

export default App;
  